// **********************************************************************
// PUCRS/FACIN
// Programa de testes para manipulação de Imagens
//
// Marcio Sarroglia Pinho
//
// pinho@pucrs.br
// **********************************************************************

#include <iostream>
#include <cmath>
#include <ccomplex>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;

#ifdef WIN32
#include <windows.h>
#include "gl\glut.h"
#endif

#ifdef __APPLE__
#include <GLUT/glut.h>
#include <vector>
#include <array>
#endif

#include "SOIL/SOIL.h"
#include "ImageClass.h"

// Guia da lib
// https://www.inf.pucrs.br/~pinho/CG-PPGCC/PraticaOpenGLImagens/ImageClass.html

char * inputFile;
ImageClass *parsedImage;
ImageClass *histogramImage = NULL;
int *peakMatrix;
long *histogramValues;
int diffValue = 70;
int **matrix;
int matrixTam, matrixMin, matrixMax;
bool showHistogram = true;

void split(const string &s, char delim, vector<string> *elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems->push_back(item);
    }
}

vector<string> * split(const string &s, char delim) {
    vector<string> *elems = new vector<string>;
    split(s, delim, elems);
    return elems;
}

/**
 * faz o parse do arquivo e retorna uma matriz com os valores
 * tamanho da matrix escrito no inteiro passado por parametro
 * enquanto le a matriz ja armazena o min e max valores nela
 */
int ** parseFile(char *fileName, int *_tam, int *_min, int *_max) {

    ifstream file(fileName);
    string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    vector<string> *lines = split(str, '\n');

    int size = lines->size();

    // assuming square images
    int **mat = (int**)calloc(size,sizeof(int*));
    for (int k = 0; k < size; ++k) {
        mat[k] = (int*)calloc(size,sizeof(int));
    }

    int min = INFINITY, max = 0;
    for (int i = 0; i < size; ++i) {
        vector<string> *pixels = split(lines->at(i), '\t');
        for (int j = 0; j < pixels->size(); ++j) {
            int val = (int)stod(pixels->at(j));
            mat[j][i] = val;
            if (val > max) max = val;
            if (val < min) min = val;
        }
    }
    *_tam = size;
    *_min = min;
    *_max = max;
    return mat;
}

/**
 * desenha pixels da matriz passada em escala de cinza em uma nova imagem com as cores respectivas aos picos.
 * imagem eh alocada e ponteiro retornado
 */
ImageClass *fromMatrixToImage(int **mat, int size) {
    ImageClass *image = new ImageClass(size, size);
    for (int l = 0; l < size; ++l) {
        for (int i = 0; i < size; ++i) {
            if (mat[l][i]>peakMatrix[0] && mat[l][i]<=peakMatrix[2]) {
                image->DrawPixel(l, i, 0,0,255);
            } else if (mat[l][i]>peakMatrix[3] && mat[l][i]<=peakMatrix[5]) {
                image->DrawPixel(l, i, 255,0,0);
            } else if (mat[l][i]>=peakMatrix[6] && mat[l][i]<=peakMatrix[8]) {
                image->DrawPixel(l, i, 0,255,0);
            } else {
                image->DrawPixel(l, i, (unsigned char)mat[l][i], (unsigned char)mat[l][i], (unsigned char)mat[l][i]);
            }
        }
    }
    return image;
}

/**
 * retorna um array com a quantidade que cada valor aparece na matriz passada.
 * @param tam o tamanho da matriz quadrada
 * @param min o valor minimo dentro da matriz
 * @param max o maior valor dentro da matriz
 */
long * histogram(int **mat, int tam, int min, int max) {
    int differentValues = max-min;
    long *values = (long*)calloc(sizeof(long),differentValues);
    for (int i = 0; i < tam; ++i) {
        for (int j = 0; j < tam; ++j) {
            int val = mat[i][j];
            if (val >=min && val <=max) {
                values[val]++;
            }
            if (val > max) values[max-1]++;
        }
    }
    return values;
}

/**
 * Converte qualquer range de valores (tipo o 0~8000) pra 0~255 Bitmap style.
 */
void normalizeValues(int **mat, int tam, int min, int max, int newMin, int newMax) {
    float factor = ((float)max-(float)min)/((float)newMax-(float)newMin);
    for (int i = 0; i < tam; ++i) {
        for (int j = 0; j < tam; ++j) {
            if (mat[i][j]!=0) {
                mat[i][j] = ((float)mat[i][j] / factor);
            } else {
                mat[i][j] = 0;
            }
        }
    }
}

float normalizeArray(long *vals, int tam, int min, int max, int newMin, int newMax) {
    float factor = ((float)max-(float)min)/((float)newMax-(float)newMin);
    for (int i = 0; i < tam; ++i) {
        if (vals[i]!=0) {
            vals[i] = ((float)vals[i] / (factor/2));
        } else {
            vals[i] = 0;
        }
    }
    return (factor/2);
}

/**
 * Seta todos valores menores que limit para zero.
 */
void removeBackground(int **mat, int tam, int limit) {
    for (int i = 0; i < tam; ++i) {
        for (int j = 0; j < tam; ++j) {
            if (mat[i][j]<limit) {
                mat[i][j] = 0;
            }
        }
    }
}

/**
 * @return aray
 * [ { min, peak, max },
 *   {                },
 *   {                } ]
 */
int *get3PeakMatrix(long *histogramValues, int tam, int start, int minDiff) {
    int matrixSize = 9;
    int *mat = (int*)calloc(sizeof(int), matrixSize);
    mat[0] = start;
    int curMatrixPosition = 1;

    long lastVal = histogramValues[start];
    long currentVal;
    bool oldDirection = true;
    for (int i = start+1; i < tam; ++i) {
        currentVal = histogramValues[i];
        bool currentDirection = lastVal<currentVal;
        if (abs(lastVal-currentVal) > minDiff){
            if (currentDirection != oldDirection) { // inverted direction
                if (curMatrixPosition < matrixSize) {
                    mat[curMatrixPosition] = i - 1;
                    curMatrixPosition++;
                    if (curMatrixPosition < matrixSize && currentDirection == true) {
                        mat[curMatrixPosition] = i;
                        curMatrixPosition++;
                    }
                }
            }
            oldDirection = currentDirection;
        }
        lastVal = currentVal;
    }
    mat[8] = 255;
    return mat;
}

/**
 * aloca uma imagem se necessario
 * retorna um histograma com os pontos de pico marcados com suas cores.
 * @param h vetor de valores do histograma
 * @param tam tamanho do histograma, sempre 256
 */
ImageClass *getHistogramImage(long *h, int tam) {
    if (histogramImage==NULL) {
        histogramImage = new ImageClass(256, 64);
    }
    long *normalizedHistogram = (long*)calloc(sizeof(long), tam);
    for (int i = 0; i < tam; ++i) normalizedHistogram[i] = h[i];
    float factor = normalizeArray(normalizedHistogram, tam, 0, matrixMax, 0, 64);
    normalizedHistogram[0] = 64;
    for (int i = 0; i < 256; ++i) { // limpando a imagem
        for (int j = 0; j < 64; ++j) {
            histogramImage->DrawPixel(i, j, 0, 0, 0);
        }
    }
    for (int j = 0; j < tam; ++j) { // desenhando o histograma inteiro
        histogramImage->DrawLine(j, (int)normalizedHistogram[j], j, 64, 255,255,255);
    }
    for (int k = 0; k < 64; k+=2) { // desenhando as linhas que dividem os picos
        histogramImage->DrawPixel(peakMatrix[0], k, 0,0,255);
        histogramImage->DrawPixel(peakMatrix[2], k, 0,0,255);
        histogramImage->DrawPixel(peakMatrix[3], k, 255,0,0);
        histogramImage->DrawPixel(peakMatrix[5], k, 255,0,0);
        histogramImage->DrawPixel(peakMatrix[6], k, 0,255,0);
        histogramImage->DrawPixel(peakMatrix[8], k, 0,255,0);
    }
    // desenhando as linhas que marcam o centro dos picos
    histogramImage->DrawLine(peakMatrix[1], (int)histogramValues[peakMatrix[1]]/factor, peakMatrix[1], 64, 0,0,255);
    histogramImage->DrawLine(peakMatrix[4], (int)histogramValues[peakMatrix[4]]/factor, peakMatrix[4], 64, 255,0,0);
    histogramImage->DrawLine(peakMatrix[7], (int)histogramValues[peakMatrix[7]]/factor, peakMatrix[7], 64, 0,255,0);
    return histogramImage;
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    float zoomH = (glutGet(GLUT_WINDOW_WIDTH))/parsedImage->SizeX();
    float zoomV = (glutGet(GLUT_WINDOW_HEIGHT))/parsedImage->SizeY();
    parsedImage->SetZoomH(zoomH);
    //parsedImage->SetZoomV(zoomV);
    parsedImage->SetPos(0,64);
    parsedImage->Display();
    histogramImage->SetZoomH(2.0);
    if (showHistogram) histogramImage->Display();
    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y) {
    if (key==27) exit(1);
    if (key=='h') showHistogram=!showHistogram;
}

void specialKeyboard(int key, int x, int y) {
    if (key==GLUT_KEY_UP) diffValue+=5;
    if (key==GLUT_KEY_DOWN) diffValue-=5;
    if (key==GLUT_KEY_PAGE_UP) diffValue+=1;
    if (key==GLUT_KEY_PAGE_DOWN) diffValue-=1;
    peakMatrix = get3PeakMatrix(histogramValues, 256, 15, diffValue);
    parsedImage = fromMatrixToImage(matrix, matrixTam);
    histogramImage = getHistogramImage(histogramValues, 256);
    cout << diffValue << endl;
}

void reshape(int w, int h) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glViewport(0, 0, w, h);
    gluOrtho2D(0,w,0,h);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int main (int argc, char** argv) {
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
    glutInitWindowPosition(10,10);

    if (argc>1) {
        inputFile = (char*)malloc(strlen(argv[1])+1);
        strcpy(inputFile, argv[1]);

        matrix = parseFile(inputFile, &matrixTam, &matrixMin, &matrixMax);
        normalizeValues(matrix, matrixTam, matrixMin, matrixMax, 0, 255);
        matrixMin = 0;
        //matrixMax = 255;
        removeBackground(matrix, matrixTam, 15);
        histogramValues = histogram(matrix, matrixTam, matrixMin, 255);
        peakMatrix = get3PeakMatrix(histogramValues, 256, 15, diffValue);

        ofstream arqsaida;
        arqsaida.open(strcat(inputFile, "_hist.txt"), ios::out );
        for (int i = 0; i < 255-matrixMin; ++i) {
            arqsaida << i << " " << histogramValues[i] << endl;
        }
        arqsaida.close();

        parsedImage = fromMatrixToImage(matrix, matrixTam);
        histogramImage = getHistogramImage(histogramValues, 256);

    } else {
        exit(1); // no arguments
    }

    // Define o tamanho da janela gráfica do programa
    glutInitWindowSize(matrixTam, matrixTam+64);
    glutCreateWindow("BRAINS");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(display);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(specialKeyboard);


    glutMainLoop();
    return 0;
}