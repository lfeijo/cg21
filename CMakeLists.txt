cmake_minimum_required(VERSION 3.5)
project(projeto)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wno-writable-strings -v -framework CoreFoundation -framework GLUT -framework OpenGL")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin")

link_directories(./lib)

set(SOURCE_FILES
        SOIL/SOIL.cpp
        SOIL/stb_image_aug.cpp
        SOIL/image_helper.cpp
        SOIL/image_DXT.cpp
        ImageClass.cpp
        main.cpp)

add_executable(projeto ${SOURCE_FILES})